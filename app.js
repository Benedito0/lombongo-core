const express = require("express");
const app = express();
const lib = require("./lib");
const bodyParser = require("body-parser");

// middleware
const urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(urlencodedParser);

const router = require("./routes/account.js"); // Import routes for "catalog" area of site

app.use("/account", router);

const PORT = process.env.PORT || 8080;
app.listen(PORT, async () => {
    console.log(`Server listening on port ${PORT}`);
    await lib.init();
});
