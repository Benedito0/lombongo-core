const admin = require("firebase-admin");
var serviceAccount = require("./lombongo-core-ci-295518-firebase-adminsdk-8ijyw-ea6277cc75.json");

const init = async () => {
    await initStore();
};

const SELF = {
    executor: null
};

const initStore = async () => {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://lombongo-core-ci-295518.firebaseio.com"
    });
};

module.exports = {
    SELF: SELF,
    init: init
};
