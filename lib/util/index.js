const config = require("./config");
const base = require("./base");

Object.assign(module.exports, config);
Object.assign(module.exports, base);
