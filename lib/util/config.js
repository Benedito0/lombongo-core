const HOST = process.env.HOST ? process.env.HOST : "127.0.0.1";
const PORT = process.env.PORT ? parseInt(process.env.PORT) : 5000;
const CICD = process.env.YUNGUIDI ? process.env.YUNGUIDI : "Local";

module.exports = {
    HOST: HOST,
    PORT: PORT,
    CICD: CICD
};
