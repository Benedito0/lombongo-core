const account = require("./account/account");
const balance = require("./account/balance");
const creditCard = require("./account/credit-card");

Object.assign(module.exports, account);
Object.assign(module.exports, balance);
Object.assign(module.exports, creditCard);
