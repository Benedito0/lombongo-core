const admin = require("firebase-admin");
const fetch = require("node-fetch");
const fs = require("fs");

exports.create = async function(req, res) {
    const db = admin.firestore();
    let response;
    try {
        const user = await admin.auth().createUser({
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.name,
            photoURL: "https://miro.medium.com/fit/c/64/64/2*06k7zJwvatGv1bGHuRsPEg.jpeg"
        });
        const uid = user.uid;
        const token = await jwt(uid);
        await db
            .collection("users")
            .doc(uid)
            .set({ balance: 30000 });
        response = { code: 200, uid: uid, token: token };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    } catch (err) {
        response = { code: 400, message: err.message };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }
};

exports.signIn = async function(req, res) {
    let response;
    try {
        const user = await admin
            .auth()
            .signInWithEmailAndPassword(req.body.email, req.body.password);
        const token = await jwt(user.uid);
        response = { code: 200, token: token };
    } catch (err) {
        response = { code: 400, message: err.message };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }
};

exports.avatar = async function(req, res) {
    let response;
    const uid = req.params.uid;
    try {
        const user = await admin.auth().getUser(uid);
        const response = await fetch(user.photoURL);
        const blob = await response.blob();
        const buf = await blob.arrayBuffer();
        res.type(blob.type);
        res.send(Buffer.from(buf));
    } catch (err) {
        response = { code: 400, message: err.message };
        res.status(400);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }
};

const jwt = async function(uid) {
    try {
        const token = await admin.auth().createCustomToken(uid);
        return token;
    } catch (err) {}
};
