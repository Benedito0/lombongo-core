const admin = require("firebase-admin");

exports.send = async function(req, res) {
    const db = admin.firestore();
    let response = {};
    let balanceRemained;
    const reciever = req.body.reciever;
    const amount = req.body.amount;
    const mandataryUid = req.body.user.uid;

    const user = await db
        .collection("users")
        .doc(mandataryUid)
        .get();

    const mandataryUsername = user.data().username;
    const transactionPicture = user.data().photoURL;

    const senderRef = db.collection("users").doc(mandataryUid);
    const recieverRef = db.collection("users").doc(reciever);

    let userObj = await senderRef.get();

    if (!userObj) {
        response = { code: 400, message: "Something went wrong" };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }

    userObj = userObj.data();

    const balance = userObj.balance;

    if (amount > balance) {
        response = { code: 400, message: "No balance available" };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }
    let createdAt;

    try {
        await db.runTransaction(async t => {
            const senderDoc = await t.get(senderRef);
            const recieverDoc = await t.get(recieverRef);

            const senderBalance = parseInt(senderDoc.data().balance);
            const recieverBalance = parseInt(recieverDoc.data().balance);

            const newSenderBalance = senderBalance - parseInt(amount);
            const newRecieverBalance = recieverBalance + parseInt(amount);
            balanceRemained = newSenderBalance;

            t.update(senderRef, { balance: newSenderBalance });
            t.update(recieverRef, { balance: newRecieverBalance });
        });

        createdAt = Date.now();

        const batch = db.batch();

        const transactionRefSender = db.collection("transactions").doc();
        const transactionRefRecieve = db.collection("transactions").doc();

        batch.set(transactionRefSender, {
            subject: mandataryUid,
            amount: parseInt(amount) * -1,
            type: "transaction_out",
            benefeciary: reciever,
            created_at: createdAt,
            avatar: transactionPicture,
            username: mandataryUsername
        });

        batch.set(transactionRefRecieve, {
            subject: reciever,
            amount: parseInt(amount),
            type: "transaction_in",
            sender: mandataryUid,
            created_at: createdAt,
            avatar: transactionPicture,
            username: mandataryUsername
        });

        await batch.commit();
    } catch (err) {
        console.log(err);
        response = { code: 400, message: err.message };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }

    response = {
        code: 200,
        message: `Transaction of ${amount} sucessful`,
        amount: amount,
        amountRemained: balanceRemained
    };
    res.status(response.code);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(response));
};
