const stripe = require("stripe")("sk_test_8AcWhHsroePIE4U83TSIq6Rf0074vHIkJi");
const admin = require("firebase-admin");

exports.createCard = async function(req, res) {
    let response = {};
    try {
        const db = admin.firestore();
        const mandataryUid = req.body.user.uid;

        const number = req.body.number;
        const expMonth = req.body.exp_month;
        const expYear = req.body.exp_year;
        const cvc = req.body.cvc;
        const card = {
            number: number,
            exp_month: expMonth,
            exp_year: expYear,
            cvc: cvc
        };

        const user = await db
            .collection("users")
            .doc(mandataryUid)
            .get();

        const stripeID = user.data().stripe;
        const cardToBeAtached = await stripe.paymentMethods.create({
            type: "card",
            card: card
        });

        await stripe.paymentMethods.attach(cardToBeAtached.id, { customer: stripeID });
    } catch (err) {
        console.log(err);
        response = { code: 400, message: err.message };
        res.status(response.code);
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify(response));
    }

    response = { code: 200, message: "Card sucessfully attached" };
    res.status(response.code);
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(response));
};
