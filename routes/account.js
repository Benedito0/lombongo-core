const express = require("express");
const router = express.Router();

const auth = require("../middlewares/auth");
const bodyParser = require("body-parser");

// middleware
const urlencodedParser = bodyParser.urlencoded({ extended: false });
router.use(urlencodedParser);

// Require controller modules.
const controller = require("../controllers");

router.post("/create", controller.create);

router.post("/send", auth, controller.send);

router.get("/login", controller.signIn);

router.get("/:uid/avatar", controller.avatar);

router.post("/credit-card/create", controller.createCard);

module.exports = router;
