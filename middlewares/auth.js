const admin = require("firebase-admin");

module.exports = async function(req, res, next) {
    const idToken = req.headers.token || req.headers.authorization;

    if (!idToken) return res.status(401).send("Access denied. No token provided.");

    try {
        const user = await admin.auth().verifyIdToken(idToken);
        req.body.user = user;
        next();
    } catch (err) {
        res.status(400).send(`Invalid token: ${err.message}`);
    }
};
